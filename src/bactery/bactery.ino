#include <WiFiManager.h>
#include <ESP8266WebServer.h>
#include "ESP8266led.h"

ESP8266WebServer server(80);

void setup() {
  // serial console conf
  Serial.begin(115200);
  // pin conf
  pinMode(LED, OUTPUT);
  digitalWrite(LED, 1);

  // run once with that line
  //SPIFFS.format();

  // wifi conf
  WiFiManager wifiManager;
  wifiManager.autoConnect("bactery", "0123456789");

  // led breathing
  ledBreathing();

  // web server conf
  SPIFFS.begin();
  {
    Dir dir = SPIFFS.openDir("/");
    while (dir.next()) {    
      String fileName = dir.fileName();
      //size_t fileSize = dir.fileSize();
      //Serial.printf("FS File: %s, size: %s\n", fileName.c_str(), formatBytes(fileSize).c_str());
      server.serveStatic(fileName.c_str(), SPIFFS, fileName.c_str());
    }
  }
  server.serveStatic("/", SPIFFS, "/index.html");
  server.begin();

  Serial.println("bactery started.");
}

void loop() {
  server.handleClient();
}
