#ifndef LEDCTRL_H
#define LEDCTRL_H

class ledCtrl
{
public:
  ledCtrl(int pin);
  void on();
  void off();
  void breathing(int times = 1, int brightness = 350, int inhaleTime = 1250, int restTime = 1000);
private:
  int _pin;
};

#endif //LEDCTRL_H
