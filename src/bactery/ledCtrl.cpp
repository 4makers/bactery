#include <Arduino.h>
#include "ledCtrl.h"

ledCtrl::ledCtrl(int pin)
 : _pin(pin)
{
  pinMode(_pin, OUTPUT);
  digitalWrite(_pin, HIGH);
}

void ledCtrl::on()
{
  digitalWrite(_pin, LOW);
}

void ledCtrl::off()
{
  digitalWrite(_pin, HIGH);
}

void ledCtrl::breathing(int times, int brightness, int inhaleTime, int restTime)
{
  int pulseTime = inhaleTime*1000/brightness;
  for (int l=0;l<times;l++) {
    if (l) delayMicroseconds(restTime*1000);
    delay(0);                             // to prevent watchdog firing.
    //ramp increasing intensity, Inhalation: 
    for (int i=1;i<brightness;i++) {
      digitalWrite(_pin, LOW);            // turn the LED on.
      delayMicroseconds(i*10);            // wait
      digitalWrite(_pin, HIGH);           // turn the LED off.
      delayMicroseconds(pulseTime-i*10);  // wait
      delay(0);                           // to prevent watchdog firing.
    }
    //ramp decreasing intensity, Exhalation (half time):
    for (int i=brightness-1;i>0;i--) {
      digitalWrite(_pin, LOW);            // turn the LED on.
      delayMicroseconds(i*10);            // wait
      digitalWrite(_pin, HIGH);           // turn the LED off.
      delayMicroseconds(pulseTime-i*10);  // wait
      i--;
      delay(0);                           // to prevent watchdog firing.
    }
  }
}
